class TopicMessageModel {
  TopicMessageModel({
    this.data,
    this.statusCode,
    this.statusMessage,
  });

  List<TopicMessageModelData> data;
  int statusCode;
  String statusMessage;

  factory TopicMessageModel.fromJson(Map<String, dynamic> json) =>
      TopicMessageModel(
        data: List<TopicMessageModelData>.from(
            json["data"].map((x) => TopicMessageModelData.fromJson(x))),
        statusCode: json["statusCode"],
        statusMessage: json["statusMessage"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "statusCode": statusCode,
        "statusMessage": statusMessage,
      };
}

class TopicMessageModelData {
  TopicMessageModelData({
    this.id,
    this.name,
    this.displayName,
    this.status,
  });

  int id;
  String name;
  String displayName;
  int status;

  factory TopicMessageModelData.fromJson(Map<String, dynamic> json) =>
      TopicMessageModelData(
        id: json["id"],
        name: json["name"],
        displayName: json["displayName"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "displayName": displayName,
        "status": status,
      };
}
