import 'dart:io';
import 'package:flutter_messaging_topic/data/models/message_model.dart';
import 'package:flutter_messaging_topic/data/models/topic_message_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class TopicMessageLocalService {
  static final TopicMessageLocalService instance =
      TopicMessageLocalService._instance();
  static Database _db;

  TopicMessageLocalService._instance();

  String messageTable = 'topic_message_table';
  String colId = 'id';
  String colName = 'name';
  String colDisplayName = 'displayName';
  String colActive = 'status';

  Future<Database> get db async {
    if (_db == null) {
      _db = await _initDb();
    }
    return _db;
  }

  Future<Database> _initDb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path + '/topic_message.db';
    final todoListDb =
        await openDatabase(path, version: 1, onCreate: _createDb);
    return todoListDb;
  }

  void _createDb(Database db, int version) async {
    await db.execute(
      '''
      CREATE TABLE $messageTable($colId INTEGER PRIMARY KEY AUTOINCREMENT,
      $colName TEXT,
      $colDisplayName TEXT,
      $colActive INTEGER)
      ''',
    );
  }

  Future<List<Map<String, dynamic>>> getTopicMessageMapList() async {
    Database db = await this.db;
    final List<Map<String, dynamic>> result = await db.query(messageTable);
    return result;
  }

  Future<List<TopicMessageModelData>> getTopicMessageList() async {
    final List<Map<String, dynamic>> topicMessageMapList =
        await getTopicMessageMapList();
    final List<TopicMessageModelData> messageList = [];
    topicMessageMapList.forEach((messageMap) {
      messageList.add(TopicMessageModelData.fromJson(messageMap));
    });

    return messageList;
  }

  Future<int> saveTopicMessage(TopicMessageModelData message) async {
    Database db = await this.db;
    final int result = await db.insert(messageTable, message.toJson());
    return result;
  }

  Future<int> updateTopicMessage(TopicMessageModelData message) async {
    Database db = await this.db;
    final int result = await db.update(
      messageTable,
      message.toJson(),
      where: '$colId = ?',
      whereArgs: [message.id],
    );
    return result;
  }

  Future<int> deleteTopicMessage() async {
    Database db = await this.db;
    final int result = await db.delete(messageTable);
    return result;
  }
}
