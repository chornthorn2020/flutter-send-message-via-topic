import 'package:flutter_messaging_topic/core/core.dart';
import 'package:flutter_messaging_topic/data/models/topic_message_model.dart';

class TopicMessageHttpService {
  final CustomHttp _http = CustomHttp();

  Future<List<TopicMessageModelData>> getTopicMessageList() async {
    final baseUrl = "https://node-js-crud.uc.r.appspot.com/api/topic_messages/";
    final response = await _http.getRequest(path: baseUrl);
    return TopicMessageModel.fromJson(response).data;
  }
}
