import 'package:flutter_messaging_topic/data/models/topic_message_model.dart';
import 'package:flutter_messaging_topic/data/services/http/topic_message_http_service.dart';
import 'package:flutter_messaging_topic/data/services/local/topic_message_local_service.dart';
import 'package:connectivity/connectivity.dart';

class TopicMessageRepository {
  final TopicMessageHttpService _serviceHttp = TopicMessageHttpService();
  final _topicMessageLocalService = TopicMessageLocalService.instance;

  Future<List<TopicMessageModelData>> getTopicList() async {
    return await _serviceHttp.getTopicMessageList();
  }

  Future<int> updateTopicMessage(TopicMessageModelData message) async {
    return await _topicMessageLocalService.updateTopicMessage(message);
  }
  Future<int> deleteTopicMessage() async {
    return await _topicMessageLocalService.deleteTopicMessage();
  }
}
