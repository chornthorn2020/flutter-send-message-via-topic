import 'package:flutter_messaging_topic/data/models/message_model.dart';
import 'package:flutter_messaging_topic/data/services/local/message_local_service.dart';

class MessageRepository {
  final _messageService = MessageLocalService.instance;

  Future<List<MessageModel>> getMessage() async {
    return await _messageService.getMessageList();
  }

  Future<int> saveMessage(MessageModel message) async {
    return await _messageService.insertMessage(message);
  }

  Future<int> updateMessage(MessageModel message) async {
    return await _messageService.updateMessage(message);
  }

  Future<int> deleteMessage(int id) async {
    return await _messageService.deleteMessage(id);
  }
}
