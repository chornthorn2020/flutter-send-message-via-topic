import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/view/pages/main_page.dart';
import 'package:flutter_messaging_topic/view/pages/messages/message_detail_page.dart';
import 'package:flutter_messaging_topic/view/pages/messages/message_page.dart';
import 'package:flutter_messaging_topic/view/pages/settings/position_page.dart';
import 'package:flutter_messaging_topic/view/pages/settings/setting_page.dart';
import 'package:flutter_messaging_topic/view/widgets/flavor/flavor_banner.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer_util.dart';

import 'core/themes/themes.dart';
import 'getx/binding/main_binding.dart';

class AppIndex extends StatelessWidget {
  const AppIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizerUtil().init(constraints, orientation);
        return FlavorBanner(
          child: Listener(
            onPointerDown: (_) {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus &&
                  currentFocus.focusedChild != null) {
                currentFocus.focusedChild.unfocus();
              }
              // Close Keyboard active
            },
            child: GetMaterialApp(
              initialRoute: '/',
              getPages: [
                GetPage(
                    name: '/', page: () => MainPage(), binding: MainBinding()),
                GetPage(name: '/message', page: () => MessagePage()),
                GetPage(name: '/setting', page: () => SettingPage()),
                GetPage(name: '/position', page: () => PositionPage()),
                GetPage(
                  name: '/message_detail',
                  page: () => MessageDetailPage(),
                ),
              ],
              theme: Themes.lightTheme,
            ),
          ),
        );
      });
    });
  }
}
