import 'package:get/get.dart';

class TopicMessageBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<TopicMessageBinding>(TopicMessageBinding());
  }
}