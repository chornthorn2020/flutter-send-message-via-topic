import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_messaging_topic/data/models/topic_message_model.dart';
import 'package:flutter_messaging_topic/data/repositories/topic_message_repository.dart';
import 'package:flutter_messaging_topic/data/services/local/topic_message_local_service.dart';
import 'package:get/get.dart';

class TopicMessageController extends GetxController {
  final _firebaseMessaging = FirebaseMessaging();
  final _topicMessageRepository = TopicMessageRepository();
  final _localService = TopicMessageLocalService.instance;

  RxBool _isLoading = false.obs;

  bool get isLoading => this._isLoading.value;
  final _topicList = <TopicMessageModelData>[].obs;

  List<TopicMessageModelData> get topicList => _topicList.value;

  @override
  void onInit() {
    //topicMessageList();
    super.onInit();
  }

  void topicMessageList() async {
    final localResData = await _localService.getTopicMessageList();
    final remoteResData = await _topicMessageRepository.getTopicList();
    _isLoading.value = true;
    if (localResData.length > 0) {
      _localService.deleteTopicMessage();
      for (TopicMessageModelData remote in remoteResData) {
        for (TopicMessageModelData local in localResData) {
          if (remote.id == local.id) {
            remote.status = local.status;
            break;
          }
        }
      }
      _topicList.value = remoteResData;
      _checkStatusAndSaveDataToLocal(_topicList);
    } else {
      _localService.deleteTopicMessage();
      _topicList.value = remoteResData;
      _checkStatusAndSaveDataToLocal(_topicList);
    }
    _isLoading.value = false;
  }

  void _checkStatusAndSaveDataToLocal(
      RxList<TopicMessageModelData> topicList) async {
    topicList.value.forEach((e) {
      _localService.saveTopicMessage(e);
      if (e.status == 1) {
        messageSubscribe(e.name);
      } else {
        messageUnSubscribe(e.name);
      }
    });
  }

  Future<void> updateTopicMessage(TopicMessageModelData message) async {
    TopicMessageModelData messageModelData = TopicMessageModelData(
      id: message.id,
      name: message.name,
      displayName: message.displayName,
      status: message.status,
    );
    await _localService.updateTopicMessage(messageModelData);
    topicMessageList();
  }

  void messageSubscribe(String topic) async {
    await _firebaseMessaging.subscribeToTopic(topic);
    print('CALL SUBSCRIBE::::: $topic');
  }

  void messageUnSubscribe(String topic) async {
    await _firebaseMessaging.unsubscribeFromTopic(topic);
    print('CALL UNSUBSCRIBE::::: $topic');
  }
}
