import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/view/pages/messages/message_page.dart';
import 'package:flutter_messaging_topic/view/pages/settings/setting_page.dart';
import 'package:get/get.dart';

class MainController extends GetxController {
  final _screenList = [MessagePage(), SettingPage()];
  int currentPageIndex = 0;

  List<Widget> get pageList => _screenList;

  final _search = TextEditingController().obs;

  TextEditingController get search => this._search.value;

  RxString _searchInputString = ''.obs;

  String get searchInputString => this._searchInputString.value;

  // Validate in search field
  void searchInputStringFun(String text) {
    this._searchInputString.value = text;
  }

  // clear when use input query string search
  void queryStringClear() {
    this.search.clear(); // clear text field
    this.searchInputStringFun('');
  }

  void onTap(int index) {
    currentPageIndex = index;
  }
}
