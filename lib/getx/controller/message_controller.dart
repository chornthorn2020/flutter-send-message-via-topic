import 'package:flutter_messaging_topic/data/models/message_model.dart';
import 'package:flutter_messaging_topic/data/repositories/message_repository.dart';
import 'package:flutter_messaging_topic/getx/controller/topic_message_controller.dart';
import 'package:get/get.dart';

class MessageController extends GetxController {
  RxBool _isLoading = false.obs;

  bool get isLoading => this._isLoading.value;

  // Message list
  final MessageRepository _messageRepository = MessageRepository();
  RxList<MessageModel> _messageList = RxList<MessageModel>();

  List<MessageModel> get messageList => _messageList.value;

  final _readMessage = MessageModel().obs;

  MessageModel get readMessage => this._readMessage.value;

  final _topicMessage = Get.find<TopicMessageController>();

  @override
  void onInit() {
    getMessage();
    _topicMessage.topicMessageList();
    super.onInit();
  }

  Future<void> getMessage() async {
    _isLoading.value = true;
    this._messageList.value = await _messageRepository.getMessage();
    _isLoading.value = false;
  }

  Future<void> deleteMessage(int id) async {
    await _messageRepository.deleteMessage(id);
    getMessage();
  }

  Future<void> saveMessage(MessageModel message) async {
    await _messageRepository.saveMessage(message);
    getMessage();
  }

  Future<void> updateMessage(MessageModel message) async {
    await _messageRepository.updateMessage(message);
    getMessage();
  }

  void onTapListTile({MessageModel data}) {
    this.readMessageFun(data);
    Get.toNamed('/message_detail', arguments: data.messageTitle);
  }

  Future<void> readMessageFun(MessageModel data) async {
    _isLoading.value = true;
    MessageModel model = MessageModel();
    model.id = data.id;
    model.messageStatus = 'R';
    model.userName = data.userName;
    model.moduleName = data.moduleName;
    model.messageSubject = data.messageSubject;
    model.messageCircleAvatar = data.messageCircleAvatar;
    model.messageTitle = data.messageTitle;
    model.messageBody = data.messageBody;
    model.createdDate = data.createdDate;
    model.updatedDate = DateTime.now();
    updateMessage(model);
    _readMessage.value = data;
    _isLoading.value = false;
    update();
  }
}
