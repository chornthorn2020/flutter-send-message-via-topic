import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_messaging_topic/data/models/message_model.dart';
import 'package:flutter_messaging_topic/data/repositories/message_repository.dart';
import 'package:flutter_messaging_topic/getx/controller/message_controller.dart';
import 'package:flutter_messaging_topic/getx/controller/notifications_controller.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

class FirebaseController extends GetxController {
  var _firebaseMessaging = FirebaseMessaging();
  final _notificationPlugin = Get.find<NotificationsController>();
  final _messageDB = Get.find<MessageController>();

  RxBool _isLoading = false.obs;

  bool get isLoading => this._isLoading.value;

  @override
  void onInit() {
    _onInitFirebaseMessaging();
    super.onInit();
  }

  void _onInitFirebaseMessaging() async {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        _notificationPlugin.showLocalNotificationAndroid(
            title: title, body: body);
        MessageModel messageModel = MessageModel(
          messageTitle: title,
          messageBody: body,
          userName: 'admin',
          moduleName: 'IA',
          messageSubject: 'sms',
          messageCircleAvatar: 'T',
          messageStatus: 'U',
          createdDate: DateTime.now(),
          updatedDate: DateTime.now(),
        );
        _messageDB.saveMessage(messageModel);
      },
      onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
  }
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    print("onBackgroundMessage: Data $message");
    print('On background message is running ');
    // Handle data message
    final dynamic data = message['data'];

    var title = message['data']['title'];
    var body = message['data']['body'];
    _saveMessageAllMode(title: title, body: body);
  }

  if (message.containsKey('notification')) {
    print("onBackgroundMessage: Data $message");
    print('On background message is running of constains key notification ');
    // Handle notification message
    final dynamic notification = message['notification'];
    var title = message['notification']['title'];
    var body = message['notification']['body'];

    _saveMessageAllMode(title: title, body: body);
  }
}

void _saveMessageAllMode({String title, String body}) async {
  MessageRepository repository = MessageRepository();
  MessageModel message = MessageModel(
    messageTitle: title,
    messageBody: body,
    userName: 'admin',
    moduleName: 'IA',
    messageSubject: 'sms',
    messageCircleAvatar: 'T',
    messageStatus: 'U',
    createdDate: DateTime.now(),
    updatedDate: DateTime.now(),
  );
  await repository.saveMessage(message);
}
