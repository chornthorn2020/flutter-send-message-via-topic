import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/view/pages/main_page.dart';

Route onGenerateRoute(RouteSettings settings) {
  final args = settings.arguments;

  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (_) => MainPage());
  }
  return onGenerateRoute(settings);
}
