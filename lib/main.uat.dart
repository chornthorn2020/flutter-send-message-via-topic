import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/config/flavors/flavor_config.dart';
import 'package:flutter_messaging_topic/config/flavors/flavor_environment.dart';
import 'package:flutter_messaging_topic/index.dart';
import 'package:get/get.dart';

import 'getx/controller/firebase_controller.dart';
import 'getx/controller/notifications_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Get.put(NotificationsController());
  Get.put(FirebaseController());
  FlavorConfig(
    environment: FlavorEnvironment.UAT,
    name: "UAT",
    color: Colors.green,
    location: BannerLocation.topStart,
    variables: {
      "baseUrl": "https://www.thorn.com",
    },
  );
  await Firebase.initializeApp();
  return runApp(AppIndex());
}
