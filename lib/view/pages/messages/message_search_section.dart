import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_messaging_topic/getx/controller/main_controller.dart';
import 'package:get/get.dart';

class MessageSearchSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.white,
      floating: true,
      elevation: 0,
      automaticallyImplyLeading: false,
      title: Container(
        height: 60,
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
        child: GetX<MainController>(
          builder: (controller) {
            return Row(
              children: [
                Expanded(
                  // flex: 5,
                  child: TextField(
                    controller: controller.search,
                    textInputAction: TextInputAction.search,
                    textCapitalization: TextCapitalization.words,
                    onChanged: (text) {
                      controller.searchInputStringFun(text);
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.zero,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      prefixIcon: Icon(Icons.search),
                      suffixIcon: controller.searchInputString.length > 0
                          ? IconButton(
                              icon: Icon(
                                Ionicons.ios_close_circle,
                                color: Colors.grey,
                              ),
                              onPressed: () => controller.queryStringClear(),
                            )
                          : null,
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
