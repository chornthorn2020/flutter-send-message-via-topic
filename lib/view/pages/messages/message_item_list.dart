import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/data/models/message_model.dart';
import 'package:flutter_messaging_topic/getx/controller/message_controller.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class MessageItemList extends StatefulWidget {
  @override
  _MessageItemListState createState() => _MessageItemListState();
}

class _MessageItemListState extends State<MessageItemList> {
  final DateFormat _dateFormat = DateFormat('MMM dd');
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: GetX<MessageController>(
              builder: (controller) {
                if (controller.isLoading) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return ListView.builder(
                  itemCount: controller.messageList.length + 1,
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    if (index < controller.messageList.length) {
                      var data = controller.messageList[index];
                      var isPortrait = MediaQuery.of(context).orientation ==
                          Orientation.portrait;
                      if (isPortrait) {
                        if (controller.messageList.length > 0) {
                          return Slidable(
                            secondaryActions: <Widget>[
                              if (data.messageStatus == 'U')
                                IconSlideAction(
                                  caption: 'Read',
                                  color: Colors.grey[300],
                                  icon: Icons.mark_email_read,
                                  onTap: () {
                                    MessageModel model = MessageModel();
                                    model.id = data.id;
                                    model.messageStatus = 'R';
                                    model.userName = data.userName;
                                    model.moduleName = data.moduleName;
                                    model.messageSubject = data.messageSubject;
                                    model.messageCircleAvatar =
                                        data.messageCircleAvatar;
                                    model.messageTitle = data.messageTitle;
                                    model.messageBody = data.messageBody;
                                    model.createdDate = data.createdDate;
                                    model.updatedDate = DateTime.now();
                                    controller.updateMessage(model);
                                  },
                                )
                              else
                                IconSlideAction(
                                  caption: 'UnRead',
                                  color: Colors.grey[300],
                                  icon: Icons.mark_as_unread,
                                  onTap: () {
                                    MessageModel model = MessageModel();
                                    model.id = data.id;
                                    model.messageStatus = 'U';
                                    model.userName = data.userName;
                                    model.moduleName = data.moduleName;
                                    model.messageSubject = data.messageSubject;
                                    model.messageCircleAvatar =
                                        data.messageCircleAvatar;
                                    model.messageTitle = data.messageTitle;
                                    model.messageBody = data.messageBody;
                                    model.createdDate = data.createdDate;
                                    model.updatedDate = DateTime.now();
                                    controller.updateMessage(model);
                                  },
                                ),
                              IconSlideAction(
                                caption: 'Delete',
                                color: Colors.red,
                                icon: Icons.delete,
                                onTap: () {
                                  controller.deleteMessage(data.id);
                                },
                              ),
                            ],
                            actionPane: SlidableDrawerActionPane(),
                            child: _buildListTilePortrait(data, controller),
                          );
                        }
                      }
                      if (controller.messageList.length > 0) {
                        return Slidable(
                          secondaryActions: <Widget>[
                            if (data.messageStatus == 'U')
                              IconSlideAction(
                                caption: 'Read',
                                color: Colors.grey[300],
                                icon: Icons.mark_email_read,
                                onTap: () {
                                  MessageModel model = MessageModel();
                                  model.id = data.id;
                                  model.messageStatus = 'R';
                                  controller.updateMessage(model);
                                },
                              )
                            else
                              IconSlideAction(
                                caption: 'UnRead',
                                color: Colors.grey[300],
                                icon: Icons.mark_as_unread,
                                onTap: () {
                                  MessageModel model = MessageModel();
                                  model.id = data.id;
                                  model.messageStatus = 'U';
                                  controller.updateMessage(model);
                                },
                              ),
                            IconSlideAction(
                              caption: 'Delete',
                              color: Colors.red,
                              icon: Icons.delete,
                              onTap: () {
                                controller.deleteMessage(data.id);
                              },
                            ),
                          ],
                          actionPane: SlidableDrawerActionPane(),
                          child: _buildListTileLandscape(data, controller),
                        );
                      } else {
                        return Center(
                          child: Text('No have data!'),
                        );
                      }
                    }
                    return Container();
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Column _buildListTileLandscape(
      MessageModel data, MessageController controller) {
    return Column(
      children: [
        SafeArea(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: CircleAvatar(
                    child: Text(
                      "S",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                flex: 15,
                child: ListTile(
                  dense: true,
                  selected: data.messageStatus == "U" ? true : false,
                  contentPadding: EdgeInsets.zero,
                  onTap: () {
                    controller.onTapListTile(data: data);
                  },
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${data.messageSubject}',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        '${_dateFormat.format(data.createdDate)}',
                        style: TextStyle(fontSize: 13),
                      ),
                    ],
                  ),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${data.messageTitle}",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "${data.messageBody}",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                      ),
                    ],
                  ),
                  isThreeLine: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Column _buildListTilePortrait(
      MessageModel data, MessageController controller) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: CircleAvatar(
                  child: Text(
                    data.messageCircleAvatar ?? "S",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 5,
              child: ListTile(
                dense: true,
                selected: data.messageStatus == "U" ? true : false,
                contentPadding: EdgeInsets.zero,
                onTap: () {
                  controller.onTapListTile(data: data);
                },
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        '${data.messageSubject}',
                        style: TextStyle(fontSize: 16),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Text(
                      '${_dateFormat.format(data.createdDate)}',
                      style: TextStyle(fontSize: 13),
                    ),
                  ],
                ),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${data.messageTitle}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "${data.messageBody}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ],
                ),
                isThreeLine: true,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
