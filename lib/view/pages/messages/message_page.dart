import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/getx/controller/notifications_controller.dart';
import 'package:flutter_messaging_topic/view/pages/messages/message_item_list.dart';
import 'package:flutter_messaging_topic/view/pages/messages/message_search_section.dart';
import 'package:get/get.dart';

class MessagePage extends StatelessWidget {
  final controller = Get.find<NotificationsController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Message Inbox'),
      ),
      body: CustomScrollView(
        slivers: [
          //MessageSearchSection(),
          MessageItemList(),
        ],
      ),
    );
  }
}
