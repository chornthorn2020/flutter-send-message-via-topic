import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:settings_ui/settings_ui.dart';

class PositionPage extends StatefulWidget {
  @override
  _PositionPageState createState() => _PositionPageState();
}

class _PositionPageState extends State<PositionPage> {
  int languageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Languages')),
      body: SettingsList(
        sections: [
          SettingsSection(tiles: [
            SettingsTile(
              title: "Flutter Developer",
              trailing: trailingWidget(0),
              onTap: () {
                changeLanguage(0, position: 'flutter');
              },
            ),
            SettingsTile(
              title: "NodeJS Developer",
              trailing: trailingWidget(1),
              onTap: () {
                changeLanguage(1, position: 'nodejs');
              },
            ),
            SettingsTile(
              title: "ReactJS Developer",
              trailing: trailingWidget(2),
              onTap: () {
                changeLanguage(2, position: 'reactjs');
              },
            ),
          ]),
        ],
      ),
    );
  }

  Widget trailingWidget(int index) {
    return (languageIndex == index)
        ? Icon(Icons.check, color: Colors.blue)
        : Icon(null);
  }

  void changeLanguage(int index, {String position}) {
    setState(() {
      languageIndex = index;
    });
  }
}
