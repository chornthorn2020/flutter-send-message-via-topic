import 'package:flutter/material.dart';
import 'package:flutter_messaging_topic/getx/controller/topic_message_controller.dart';
import 'package:get/get.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select Topic'),
        centerTitle: true,
      ),
      body: GetX<TopicMessageController>(
        builder: (controller) {
          if (controller.isLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (controller.topicList.length > 0) {
            return ListView.builder(
              itemCount: controller.topicList.length,
              itemBuilder: (context, index) {
                final data = controller.topicList[index];
                return CheckboxListTile(
                  title: Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Text(data.displayName),
                  ),
                  value: data.status == 1,
                  onChanged: (bool value) {
                    data.status = value ? 1 : 0;
                    controller.updateTopicMessage(data);
                  },
                );
              },
            );
          }

          return Container();
        },
      ),
    );
  }
}
